#!/usr/bin/env bash

if [ ! -f ./phpcs.phar ]; then
    echo "*** php code sniffer phar does not exists!!"
    exit 1
fi

echo "start running php code sniffer"
php phpcs.phar --standard=PSR12 --ignore=index.php --report=json src/ > ./php_code_sniffer_report.json

if [ ! -f ./php_code_sniffer_report.json ]; then
    echo "*** php code sniffer report does not exists!!"
    exit 1
fi

echo "Writing report..."
php .gitlab/ci/code_quality_report.php > ./gl-code-quality-report.json

if [ ! -f ./gl-code-quality-report.json ]; then
    echo "*** code climate report file does not exists!!"
    exit 1
fi

exit 0